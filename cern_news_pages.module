<?php

use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\Views;

const INTERNAL_NEWS_TERM_NAME = 'CERN community';


/**
 * Implements hook_form_alter() to add classes to the search form.
 */
function cern_news_pages_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if ($form_id == 'node_news_page_edit_form' || $form_id == 'node_news_page_form') {

    // Hide - show fields for image
    $form['field_p_news_display_cds']['#states'] = [
      'visible' => [
        'select[name="field_p_news_display_media_type"]' => ['value' => 'cds']
      ]
    ];

    $form['field_p_news_display_image']['#states'] = [
      'visible' => [
        'select[name="field_p_news_display_media_type"]' => ['value' => 'image']
      ]
    ];

    $form['field_p_news_display_caption']['#states'] = [
      'visible' => [
        'select[name="field_p_news_display_media_type"]' => ['value' => 'image']
      ]
    ];

    // Hide / Show listing media
    $form['field_p_news_display_list_cds']['#states'] = [
      'visible' => [
        'select[name="field_p_news_display_list_media"]' => ['value' => 'cds']
      ]
    ];

    $form['field_p_news_display_listing_img']['#states'] = [
      'visible' => [
        'select[name="field_p_news_display_list_media"]' => ['value' => 'image']
      ]
    ];

    // Hide term (News Format) distinct from "Press release" and "News" if the user has the press role
    $user = \Drupal::currentUser()->getRoles();
    if (isset($user)){
      if (in_array('press_officer', $user)) {
        $form['#attached']['library'][] = 'cern_news_pages/cernnewspages';
        if (isset($form['field_p_news_display_news_format']['widget']['#options'])) {
          foreach ($form['field_p_news_display_news_format']['widget']['#options'] as $key => $value) {
            if (($value != 'Press release') && ($value != 'News')) {
              unset($form['field_p_news_display_news_format']['widget']['#options'][$key]);
            }
          }
        }
      }
    }
  }
}


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Replace "Any" by "Everyone" on all news view audience selector.
 */
function cern_news_pages_form_views_exposed_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if ($form['#id'] == 'views-exposed-form-cern-all-news-all-news') {
    $form['audience']['#options']['All'] = t('- Everyone -');
  }
}


/**
 * Implements hook_view_post_render().
 *
 * Header links General news and Internal news.
 */
function cern_news_pages_views_post_render(ViewExecutable $view, &$output, CachePluginBase $cache) {
  if ($view->storage->get('id') === 'cern_all_news') {
    $audience = \Drupal::request()->get('audience');
    if ($audience == 'All') {
      $audience = '';
    }
    $term = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => INTERNAL_NEWS_TERM_NAME, 'vid' => 'cern_audience']);

    $internal_news_tid = key($term);
    // Check if the handler object exists before modifying its properties.
    if (isset($view->display['page']->handler->handlers['header']['area'])) {
      $view->display['page']->handler->handlers['header']['area']->options['content'] = '
        <a href="/news" '. (empty($audience) ? 'class="active"' : '' ).' >'.t('General news').'</a>
      | <a href="/news?audience='.$internal_news_tid .'" '. ($audience == $internal_news_tid ? 'class="active"' : '').' >'.t('Internal news') .'</a>';
    }
  }
}

/**
 * Update the length of a text field which already contains data.
 *
 * @param string $entity_type_id
 * @param string $field_name
 * @param integer $new_length
 */
function _cern_news_pages_change_text_field_max_length(string $entity_type_id, string $field_name, int $new_length) {
  $name = 'field.storage.' . $entity_type_id . "." . $field_name;

  // Get the current settings
  $result = \Drupal::database()->query(
    'SELECT data FROM {config} WHERE name = :name',
    [':name' => $name]
  )->fetchField();
  $data = unserialize($result);
  $data['settings']['max_length'] = $new_length;

  // Write settings back to the database.
  \Drupal::database()->update('config')
    ->fields(['data' => serialize($data)])
    ->condition('name', $name)
    ->execute();

  // Update the value column in both the _data and _revision tables for the field
  $table = $entity_type_id . "__" . $field_name;
  $table_revision = $entity_type_id . "_revision__" . $field_name;
  $new_field = ['type' => 'varchar', 'length' => $new_length];
  $col_name = $field_name . '_value';
  \Drupal::database()->schema()->changeField($table, $col_name, $col_name, $new_field);
  \Drupal::database()->schema()->changeField($table_revision, $col_name, $col_name, $new_field);

  // Flush the caches.
  drupal_flush_all_caches();
}

/**
 * Implements hook_update_N().
 */
function cern_news_pages_update_8101(&$sandbox) {
  _cern_news_pages_change_text_field_max_length('node', 'field_p_news_display_strap', 255);
}

/**
 * Implements hook_views_query_alter().
 *
 * Add date filters from 2 content types: news_page and feature_story_page.
 */
function cern_news_pages_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->storage->get('id') === 'cern_all_news') {
    $field_publication = "
      CASE
        WHEN type = 'news_page' THEN field_p_news_display_pub_date_value
        WHEN type = 'feature_story_page' THEN field_p_feature_story_page_date_value
      END";

    $configuration = array(
      'type'       => 'LEFT',
      'table'      => 'node__field_p_feature_story_page_date',
      'field'      => 'entity_id',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
      'operator'   => '=',
      'extra'      => "node__field_p_feature_story_page_date.deleted  = '0' AND node__field_p_feature_story_page_date.langcode = node_field_data.langcode",
    );

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $query->addTable('node__field_p_feature_story_page_date', 'node_field_data', $join);
    $query->addField(NULL, $field_publication, 'publication');
    $query->orderby = array([
      'field' => 'publication',
      'direction' => 'DESC',
    ]);

    // Filter
    $date_from = \Drupal::request()->get('date_from');
    $date_to = \Drupal::request()->get('date_to');
    if (!empty($date_from) || !empty($date_to)){
      if (isset($query->where[2])) {
        $query->where[2]['conditions'] = [];
      }
      if (isset($query->where[3])) {
        $query->where[3]['conditions'] = [];
      }
    }

    $date_field_groups_from = [];
    if (!empty($date_from)) {
      $date_field_groups_from[] = ['field_p_news_display_pub_date' => '>='];
      $date_field_groups_from[] = ['field_p_feature_story_page_date' => '>='];
    }

    $date_field_groups_to = [];
    if (!empty($date_to)) {
      $date_field_groups_to[] = ['field_p_news_display_pub_date' => '<='];
      $date_field_groups_to[] = ['field_p_feature_story_page_date' => '<='];
    }

    $conditions = "";
    if (is_array($date_field_groups_from) && count($date_field_groups_from) > 0) {
      foreach ($date_field_groups_from as $key => $fields) {
        $conditions .= ($key==0) ? "" : " OR ";
        $condition_value = $date_from;
        foreach ($fields as $field_name => $operator) {
          $conditions .= " DATE_FORMAT((node__".$field_name.".".$field_name."_value  + INTERVAL 7200 SECOND), '%Y-%m-%d\T%H:%i:%s') ".$operator." DATE_FORMAT(('".$condition_value."' + INTERVAL 7200 SECOND), '%Y-%m-%d\T%H:%i:%s') ";
        }
      }
      $query->addWhereExpression(2, $conditions);
    }

    if (is_array($date_field_groups_to) && count($date_field_groups_to) > 0) {
      $conditions = "";
      foreach ($date_field_groups_to as $key => $fields) {
        $conditions .= ($key==0) ? "" : " OR ";
        $condition_value = $date_to;
        foreach ($fields as $field_name => $operator) {
          $conditions .= " DATE_FORMAT((node__".$field_name.".".$field_name."_value  + INTERVAL 7200 SECOND), '%Y-%m-%d\T%H:%i:%s') ".$operator." DATE_FORMAT(('".$condition_value."' + INTERVAL 7200 SECOND), '%Y-%m-%d\T%H:%i:%s')";
        }
      }
      $query->addWhereExpression(2, $conditions);
    }
  }
}
