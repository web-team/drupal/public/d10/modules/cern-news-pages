# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.2] - 06/12/2023

- Updated `allowed_formats` to version `^3.0`.
- Updated `paragraphs` to version `^1.16`.
- Updated `pathauto` to version `^1.12`.
- Updated `scheduler` to version `^2.0`.

## [3.0.1] - 08/03/2023
- Fix `handler` being `null` issue.

## [3.0.0] - 07/02/2023
- Made PHP 8.1 compatible.
- Made Drupal &gt;=9.5.1 compatible.
- Updated all dependencies, including CERN ones, to the newest available.
- Remove `social_media` dependency.

## [2.0.5] - 10/12/2021

- Removed deprecated call `getCurrentUserId` from `core.base_field_override.node.news_page.uid.yml` configuration
- New call is `default_value_callback: 'Drupal\node\Entity\Node::getDefaultEntityOwner'`

## [2.0.4] - 08/12/2021

- Fix deprecated PHP function for `$view`

## [2.0.3] - 29/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.0.2] - 13/10/2021

- Add D9 readiness

## [2.0.1] - 28/05/2021

- Add composer support
- Add CHANGELOG
